import sleep from '../lib/sleep'

export default async (req, res) => {
  await sleep(300)
  res.send('job2').end()
}