import axiosInstance from './axiosInstance'
import sleep from './sleep'

const duration = 5000
const baseURL = axiosInstance.defaults.baseURL || 'https://freshfaces.vercel.app'
const timeout = 9000

const delegate = async (url, wait) => {
  await sleep(wait)
  await axiosInstance.get(url, { baseURL })
}

export default async function ping(url, req, res) {
  const now = Date.now()

  res.setHeader('Cache-Control', 's-maxage=4, stale-while-revalidate')

  const ip = req.headers['x-forwarded-for'] ||
    req.connection.remoteAddress ||
    req.socket.remoteAddress

  const remainder = new Date(now).getSeconds() * 1000 % duration
  const adjustment = remainder === 0 ? 0 : 1000
  const wait = duration - new Date(now).getMilliseconds() + adjustment

  console.log(new Date(), 'Ping', url, 'IP', ip)

  await Promise.race([delegate(url, wait), sleep(timeout)])

  res.send(`PING ${new URL(url, baseURL)} [${Date.now() - now}ms]`)
}